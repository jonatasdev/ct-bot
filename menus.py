from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
import json

class Menus: 
    def menu_livros(serie):
        markup = InlineKeyboardMarkup()
        series = open(f'./series/{serie}.json','r')
        for serie in series:
            botao = InlineKeyboardButton(serie, callback_data=serie)
            markup.add(botao)
        return markup

    def menu_series():
        markup = InlineKeyboardMarkup()
        with open('./series/series.json') as file:
            lista = json.load(file)
            series = lista["series"]
            for serie in series:
                text = serie["nome"]
                btn = InlineKeyboardButton(text, callback_data=text)
                markup.add(btn)
        back_btn = InlineKeyboardButton("VOLTAR",callback_data="voltar_menu_principal")
        markup.add(back_btn)
        return markup

    def menu_principal():
        markup = InlineKeyboardMarkup()
        botao_livros = InlineKeyboardButton("Livros", callback_data="botao_livros")
        botao_atividades = InlineKeyboardButton("Atividades", callback_data="botao_atividades")
        botao_horario = InlineKeyboardButton("Horário", callback_data="botao_horario")
        botao_dev = InlineKeyboardButton("Desenvolvedor e Mantenedor", url="t.me/jonatasdev")
        markup.add(botao_livros)
        markup.add(botao_atividades, botao_horario)
        markup.add(botao_dev)
        return markup
