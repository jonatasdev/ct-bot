# Bot para telegram da escola EEFM Maria da Conceição Porfírio Teles

- Criei esse bot com o intuito de ajudar a galera, mas não deu muito certo, ninguém usa. Então deixei para meu grupo de amigos privado, pelo menos alguma utilidade esse negócio tem que ter.

# Funções

- Livros (Passível de adicionar diversas séries)
- Horário sempre atualizado (necessidade de intervenção administrativa)
- Atividades (Todas feitas/enviadas por voluntários)

# Ainda falta adicionar

- Funções para adicionar séries ou turmas específicas pelo bot mesmo
- Adicionar atividades de forma rápida e fácil para facilitar a contribuição da comunidade
- Biblioteca, onde terão uma série de livros disponíveis
- Materiais de apoio de atividades, tais como redação, matemática e português
- Quiz aluno, onde será sorteado um quiz de alguma materia em específico para que o aluno tente acertar
- Suporte inline para acessar certas funções do bot em qualquer lugar
