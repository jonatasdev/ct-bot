import telebot
from menus import Menus

token = "6087350373:AAEH6QUhOzEHjYL53Bg0tSziGHKRAm6vXFc"

bot = telebot.TeleBot(token, parse_mode="markdown")

texto_menu_principal = "Navegue em minhas funções pelos botões abaixo."

@bot.callback_query_handler(func=lambda call: True)
def resposta_botoes(call):
    if call.data == "voltar_menu_principal":
        bot.edit_message_text(texto_menu_principal, call.from_user.id, call.message.id, reply_markup=Menus.menu_principal())
    if call.data == "1-Ano":
        bot.edit_message_text("Livros do 1 ano disponíveis", call.from_user.id, call.message.id, reply_markup=Menus.menu_livros(call.data))
    if call.data == "botao_livros":
        bot.edit_message_text("Selecione uma das séries disponíveis: ", call.from_user.id, call.message.id, reply_markup=Menus.menu_series())

@bot.message_handler(commands=["start"])
def start(message):
    bot.reply_to(message, texto_menu_principal, reply_markup=Menus.menu_principal())

@bot.message_handler(commands=["aviso"])
def aviso(message):
    chats = bot.get_updates()[-1].message.chat
    for chat in chats:
        bot.send_message(chat.id, "envio de aviso em massa.")

bot.infinity_polling()
